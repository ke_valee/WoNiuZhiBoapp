package com.stylefeng.guns.rest.common.persistence.dao;

import com.stylefeng.guns.api.live.vo.TypeListVO;
import com.stylefeng.guns.rest.common.persistence.model.SnailClass;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author qifanlee
 * @since 2019-04-30
 */
public interface SnailClassMapper extends BaseMapper<SnailClass> {

    @Select("select * from snail_class")
    List<TypeListVO> getTypeList();
}
