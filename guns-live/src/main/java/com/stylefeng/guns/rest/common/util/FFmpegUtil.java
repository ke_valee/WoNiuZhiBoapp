package com.stylefeng.guns.rest.common.util;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.util.Date;

@Data
@Slf4j
@Configuration
@ConfigurationProperties(prefix = "ffmpeg")
public class FFmpegUtil {
    //房间号
//    private String guid;
    private String picPath;
    private String liveStream;

    /**
     * 对直播流进行截图
     */
    public String Screenshot(String guid) {
        // 获得时间戳文件名
        String fileStamp = guid + "_" + new Date().getTime();
        // 重命名原来的文件 结合timestamp
        String cmd = "mv " + picPath + guid + ".png " + picPath + fileStamp + ".png";
        System.out.println(cmd);
        //直播流路径
        String Stream = liveStream + guid;
        //拼接ffmpeg命令行
        //下面命令表示从 rtmp://192.168.124.129/myapp/10000 视频流的2秒后切一个图
        String cutCmd = "ffmpeg -i " + Stream + " -f image2 -ss 1 -vframes 1 -s 510*300 "
                + picPath + guid + ".png";
        System.out.println(cutCmd);
        Runtime runtime = Runtime.getRuntime();
        Process process = null;
        Process process2 = null;
        try {
            process = runtime.exec(cmd);
            int exitValue = process.waitFor();
            if (0 != exitValue) {
                log.debug("=============================================call shell failed. error code is :"
                        + exitValue);
             }
            process2 = runtime.exec(cutCmd);
            int exitValue2 = process2.waitFor();
            if (0 != exitValue2) {
                log.debug("================failed. error code is :"
                        + exitValue2);
            }
        } catch (IOException e) {
            log.error("发生错误" + e);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            process.destroy();
            process2.destroy();
        }
        return null;
    }
}
