package com.stylefeng.guns.rest.modular.live.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import com.stylefeng.guns.api.live.RedisSortAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.Set;


@Component
@Service(interfaceClass = RedisSortAPI.class)
public class DefaultRedisSortServiceImpl implements RedisSortAPI {

    private static final  String  ZSET = "zset";

    @Resource
    private RedisSortUtil redisService;

    /**
     * 根据key 进行缓存操作
     * @param key
     */
    @Override
    public void sort(String key){
        Long rank2 = redisService.rank(ZSET, key);
        // 如果rank2为null,则缓存里面不存在该值
        if(null == rank2){
            // 不存在,则添加,默认排序为1
            redisService.zAdd(ZSET,key,1.0);
            System.out.println("当前:"+key +":的搜索次数为"+1);
        }else {
            // 如果存在,则获取排序值  并且+1
            int score = (int)redisService.score(ZSET, key);
            System.out.println("当前:"+key +":的搜索次数为"+(score+1));
            redisService.zAdd(ZSET,key,score+1);
        }
    }

    /**
     * 从高到低的排序集中获取从头(start)到尾(end)内的元素。
     * @param start 0 表示第一个
     * @param end  -1 表示最后
     * @return
     */
    @Override
    public Object[] findZset(long start, long end){
        Set<Object> objects = redisService.reverseRange(ZSET, start, end);
        Object[] objects1 = objects.toArray();
        return objects1;

    }
}

