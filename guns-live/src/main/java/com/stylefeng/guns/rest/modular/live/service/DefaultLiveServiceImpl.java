package com.stylefeng.guns.rest.modular.live.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.api.live.LiveServiceAPI;
import com.stylefeng.guns.api.live.vo.*;
import com.stylefeng.guns.api.user.vo.UserInfoModel;
import com.stylefeng.guns.rest.common.persistence.dao.SnailClassMapper;
import com.stylefeng.guns.rest.common.persistence.dao.SnailStudioMapper;
import com.stylefeng.guns.rest.common.persistence.model.SnailClass;
import com.stylefeng.guns.rest.common.persistence.model.SnailStudio;
import com.stylefeng.guns.rest.common.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Component
@Service(interfaceClass = LiveServiceAPI.class,timeout = 6000)
public class DefaultLiveServiceImpl implements LiveServiceAPI {

    @Autowired
    private SnailStudioMapper snailStudioMapper;

    @Autowired
    private SnailClassMapper snailClassMapper;

    @Autowired
    private FFmpegUtil ffmpegUtil;

    /**
     * 对正在直播的直播间进行视频流截图
     * - 运行system linux命令
     * - 保存的名称就是直播间的房间号的名称
     * - 覆盖掉之前的文件
     */
    @Override
    public String ffmpegPhoto(){

        //查询所有正在直播的直播间及其房间号
        EntityWrapper entityWrapper = new EntityWrapper();
        entityWrapper.eq("status","1");
        List<SnailStudio> snailStudios= snailStudioMapper.selectList(entityWrapper);
        // 循环执行ffmpeg 截图功能
        for (SnailStudio snailStudio : snailStudios){
            ffmpegUtil.Screenshot(snailStudio.getGuid()+"");
        }
        return null;
    }

    /**
     * 获取分类列表
     * @return
     */
    @Override
    public List<TypeListVO> getTypeList() {
        List<TypeListVO> typeList = snailClassMapper.getTypeList();
        return typeList;
    }

    @Override
//    @Cacheable(value = "liveCache",key = "'liveIndex'")
    public IndexVO getIndexInfo() {
        //全部直播
        List<StudioVO> studioVOList = new ArrayList<>();
        //星秀类
        List<StudioVO> xingxiuVOList = new ArrayList<>();

        //查询所有正在直播的直播间及其房间号
        EntityWrapper entityWrapper = new EntityWrapper();
        entityWrapper.eq("status","1");
        List<SnailStudio> snailStudios= snailStudioMapper.selectList(entityWrapper);
        List<TypeListVO> typeList = getTypeList();
        List<StudioVO> studioVOByType = getLiveByType(1);
        for (SnailStudio snailStudio:snailStudios){
            StudioVO studioVO = new StudioVO();
            studioVO.setGuid(snailStudio.getGuid()+"");
            studioVO.setPicPath("http://47.100.244.66/product/pictures/"+snailStudio.getGuid()+".png");
            studioVO.setLiveStream(ffmpegUtil.getLiveStream()+snailStudio.getGuid());
            studioVO.setContent(snailStudio.getContent());
            studioVO.setType(snailStudio.getType());
            studioVO.setOnlineNum(snailStudio.getOnlineNum());
            studioVOList.add(studioVO);
        }
        IndexVO indexVO = new IndexVO();
        indexVO.setTypeList(typeList);
        indexVO.setStudioVOList(studioVOList);
        indexVO.setXingxiuVOList(studioVOByType);
        return indexVO;
    }

    /**
     * 获取对应分类的直播
     * @param i
     * @return
     */
    @Override
    public List<StudioVO> getLiveByType(int i) {
        //DO
        List<SnailStudio> liveByType = new ArrayList<>();
        //VO
        List<StudioVO> studioVOList = new ArrayList<>();
        if(i == 99){
            EntityWrapper entity = new EntityWrapper();
            entity.eq("status",1);
            liveByType = snailStudioMapper.selectList(entity);
        }else{
            liveByType = snailStudioMapper.getLiveByType(i);
        }
        //do转换为vo
        for(SnailStudio snailStudio:liveByType){
            StudioVO studioVO = new StudioVO();
            studioVO.setContent(snailStudio.getContent());
            studioVO.setType(snailStudio.getType());
            studioVO.setOnlineNum(snailStudio.getOnlineNum());
            studioVO.setGuid(snailStudio.getGuid()+"");
            studioVO.setLiveStream(ffmpegUtil.getLiveStream()+snailStudio.getGuid());
            studioVO.setPicPath("http://47.100.207.121/product/pictures/"+snailStudio.getGuid()+".png");
            studioVOList.add(studioVO);
        }
        return studioVOList;
    }

    //生成随机数
    private String getCard(){
        Random rand=new Random();
        //生成随机数
        String cardNnumer="";
        for(int a=0;a<8;a++){
            cardNnumer+=rand.nextInt(10);
        }
        //生成八位数字
        return cardNnumer;
    }

    @Override
    //开启直播创建直播间
    @Transactional
    public int careteLive(Integer uuid,String content,int type) {
        SnailStudio snailStudio = new SnailStudio();
        snailStudio.setGuid(Integer.parseInt(this.getCard()));
        snailStudio.setUid(uuid);
        snailStudio.setStatus(1);
        snailStudio.setType(type);
        if(content!=null && content.length()>0){
            snailStudio.setContent(content);
        }
        Integer res = snailStudioMapper.insert(snailStudio);
        if(res > 0){
            return snailStudio.getGuid();
        }else{
            return 0;
        }
    }

    @Override
    public int updateAnchor(UserInfoModel user) {

        return snailStudioMapper.updateAnchor(user);
    }

    @Override
    public int closeLive(Integer uuid) {
        int status =2;
        int type = 1;
        if(uuid != null && uuid >0){
            int i = snailStudioMapper.updateStatus(uuid,status,type);
            if(i>0){
                return i;
            }
        }
        return 0;
    }

    @Override
    public Integer checkLiveByUid(int uid) {
        SnailStudio snailStudio = new SnailStudio();
//        SnailStudio studio = snailStudioMapper.getStudioByUid(uid);
        Integer studioByUid = snailStudioMapper.getStudioByUid(uid);
        if(studioByUid == null){
            return 0;
        }else{
            return studioByUid;
        }
    }

    @Override
    public int updateStatus(int uuid,int type) {
        int status =1;
        int i = snailStudioMapper.updateStatus(uuid, status,type);
        return i;
    }

    @Override
    public int updateContent(int uuid,String content) {

        return snailStudioMapper.updateContent(uuid,content);
    }

    @Override
    public SearchResultsVO searchByKeyword(String keyword) {
        //根据账号精确搜索用户
        List<RelatedUserVO> userByUserName = snailStudioMapper.getUserByUserName(keyword);
        if(userByUserName.size() >= 1){
            SearchResultsVO searchResultsVO = new SearchResultsVO();
            searchResultsVO.setRelatedUser(userByUserName);
            return searchResultsVO;
        }
        //根据房间号精确搜索直播
        List<StudioVO> liveByGuid = snailStudioMapper.getLiveByGuid(keyword);
        if(liveByGuid.size() >= 1){
            for(StudioVO studioVO : liveByGuid){
                studioVO.setPicPath("http://47.100.244.66/product/pictures/"+studioVO.getGuid()+".png");
                studioVO.setLiveStream(ffmpegUtil.getLiveStream()+studioVO.getGuid());
            }
            SearchResultsVO searchResultsVO = new SearchResultsVO();
            searchResultsVO.setRelatedLive(liveByGuid);
            return searchResultsVO;
        }

        List<RelatedUserVO> relatedUserVO = null;
        List<RelatedHostVO> relatedHostVO = null;
        List<StudioVO> studioVOList=null;
        relatedUserVO = snailStudioMapper.getUserByNickname("%"+keyword+"%");
        relatedHostVO = snailStudioMapper.getHostByNickname("%"+keyword+"%");
        studioVOList = snailStudioMapper.getLiveByContent("%"+keyword+"%");
        for(StudioVO studioVO : studioVOList){
            studioVO.setPicPath("http://47.100.244.66/product/pictures/"+studioVO.getGuid()+".png");
            studioVO.setLiveStream(ffmpegUtil.getLiveStream()+studioVO.getGuid());
        }
        SearchResultsVO searchResultsVO = new SearchResultsVO();
        searchResultsVO.setRelatedHost(relatedHostVO);
        searchResultsVO.setRelatedUser(relatedUserVO);
        searchResultsVO.setRelatedLive(studioVOList);

        return searchResultsVO;
    }

    @Override
    public List<StudioVO> getWatchRecord(int userId) {

        return null;
    }


}
