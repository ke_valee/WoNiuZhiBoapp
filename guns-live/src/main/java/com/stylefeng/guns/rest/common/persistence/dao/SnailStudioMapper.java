package com.stylefeng.guns.rest.common.persistence.dao;

import com.stylefeng.guns.api.live.vo.RelatedHostVO;
import com.stylefeng.guns.api.live.vo.RelatedUserVO;
import com.stylefeng.guns.api.live.vo.StudioVO;
import com.stylefeng.guns.api.user.vo.UserInfoModel;
import com.stylefeng.guns.rest.common.persistence.model.SnailStudio;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * <p>
 * 直播间表 Mapper 接口
 * </p>
 *
 * @author qifanlee
 * @since 2019-04-01
 */
public interface SnailStudioMapper extends BaseMapper<SnailStudio> {

    //更改直播状态
    @Update("update snail_studio set status = #{status},type=#{type} where uid = #{uuid}")
    int updateStatus(@Param("uuid") int uuid, @Param("status") int status,@Param("type")int type);

    //更改直播间描述
    @Update("update snail_studio set content = #{content} where uid = #{uuid}")
    int updateContent(@Param("uuid") int uuid, @Param("content") String content);

    //根据关键字搜索相关用户
    List<RelatedUserVO> getUserByNickname(String keyword);

    //根据关键字搜索相关主播
    List<RelatedHostVO> getHostByNickname(String keyword);

    //根据账号精确搜索用户
    List<RelatedUserVO> getUserByUserName(String keyword);
    //根据房间号精确搜索直播
    List<StudioVO> getLiveByGuid(String keyword);

    //根据关键字搜索相关直播
    List<StudioVO> getLiveByContent(String keyword);

    //获取对应分类的直播
    @Select("select * from snail_studio where type = #{i} and status = 1")
    List<SnailStudio> getLiveByType(int i);

    //更改用户身份为主播
    @Update("update snail_user set anchor = #{anchor} where UUID = #{uuid}")
    int updateAnchor(UserInfoModel user);

    //根据用户id搜索直播间
    @Select("select guid from snail_studio where uid = #{uid}")
    Integer getStudioByUid(int uid);
}

