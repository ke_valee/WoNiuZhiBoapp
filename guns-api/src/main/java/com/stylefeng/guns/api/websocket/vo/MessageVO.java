package com.stylefeng.guns.api.websocket.vo;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "chat")
public class MessageVO {

    @Id
    private String id;
    private String fromUser;//发送者.
    private String msg;//消息
    private String time;//时间
    private int onlineNum; //当前在线人数

    @Override
    public String toString() {
        return "id:" + id  +
                ", fromUser:" + fromUser +
                ", msg:" + msg  +
                ", time:" + time  +
                ", onlineNum:" + onlineNum;
    }
}