package com.stylefeng.guns.api.live;

import java.util.Set;

public interface RedisSortAPI {
    //热词缓存
    public void sort(String key);
    //热词排序
    public Object findZset(long start, long end);
}
