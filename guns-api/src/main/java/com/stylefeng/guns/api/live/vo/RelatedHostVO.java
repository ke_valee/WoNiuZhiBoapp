package com.stylefeng.guns.api.live.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class RelatedHostVO implements Serializable {
    private  int uuid;
    //头像地址
    private String headAddr;
    //昵称
    private String nickname;
    //房间号
    private String guid;
    //直播状态
    private String status;
    //关注度
    private int attention;
}
