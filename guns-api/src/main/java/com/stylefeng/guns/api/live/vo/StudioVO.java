package com.stylefeng.guns.api.live.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 直播间信息
 */
@Data
public class StudioVO implements Serializable {

    //房间号
    private String guid;

    //图片地址
    private String picPath;

    //直播内容简介
    private String content;

    //直播流地址
    private  String liveStream;

    //直播类型
    private int type;

    //观看人数
    private int onlineNum;

}
