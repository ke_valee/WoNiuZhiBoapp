package com.stylefeng.guns.api.live.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class IndexVO implements Serializable {
    //分类列表
    private List<TypeListVO> typeList;

    //全部
    private List<StudioVO> studioVOList;

    //星秀类
    private List<StudioVO> xingxiuVOList;


}
