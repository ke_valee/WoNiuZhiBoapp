package com.stylefeng.guns.api.websocket.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class RequestVO implements Serializable {
    private String sender;//消息发送者
    private String room;//房间号
    private String type;//消息类型
    private String content;//消息内容

    public RequestVO() {
    }
    public RequestVO(String sender,String room, String type, String content) {
        this.sender = sender;
        this.room = room;
        this.type = type;
        this.content = content;
    }

}
