package com.stylefeng.guns.api.user.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserValidRequestVO implements Serializable {

    private String realname;
    private String idcard;
}
