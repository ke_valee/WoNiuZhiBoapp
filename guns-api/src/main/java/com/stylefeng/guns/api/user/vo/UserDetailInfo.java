package com.stylefeng.guns.api.user.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserDetailInfo implements Serializable {
    private String username;
    private String nickname;
    private String head_path;
    private Integer age;
    private String sex;
    private String birthday;
    private String address;
    private Integer fansNum;
    private Integer focusNum;
    private String room_id;
}
