package com.stylefeng.guns.api.live;

import com.stylefeng.guns.api.live.vo.IndexVO;
import com.stylefeng.guns.api.live.vo.SearchResultsVO;
import com.stylefeng.guns.api.live.vo.StudioVO;
import com.stylefeng.guns.api.live.vo.TypeListVO;
import com.stylefeng.guns.api.user.vo.UserInfoModel;

import java.util.List;
import java.util.Set;

public interface LiveServiceAPI {
    //ffmpeg流截图
    String ffmpegPhoto();

    //获取分类列表
    List<TypeListVO> getTypeList();

    //获取首页信息
    IndexVO getIndexInfo();

    //对应分类的直播
    List<StudioVO> getLiveByType(int i);

    //开启直播（创建直播间）
    int careteLive(Integer uuid,String content,int type);

    //更改用户身份为主播
    int updateAnchor(UserInfoModel user);

    //关闭直播
    int closeLive(Integer uuid);

    //用户是否已有直播间
    Integer checkLiveByUid(int uid);

    //更改直播状态
    int updateStatus(int uid,int type);

    //更改直播间描述
    int updateContent(int uid,String content);

    //关键字搜索
    SearchResultsVO searchByKeyword(String keyword);

    //获取浏览记录
    List<StudioVO> getWatchRecord(int userId);
}
