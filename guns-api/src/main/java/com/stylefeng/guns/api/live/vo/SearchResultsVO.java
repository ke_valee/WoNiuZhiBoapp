package com.stylefeng.guns.api.live.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SearchResultsVO implements Serializable {
    private List<RelatedUserVO> relatedUser;
    private List<RelatedHostVO> relatedHost;
    private List<StudioVO> relatedLive;
}
