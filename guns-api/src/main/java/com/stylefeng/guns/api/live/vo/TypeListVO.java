package com.stylefeng.guns.api.live.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class TypeListVO implements Serializable {
    private Integer id;
    /**
     * 分类名
     */
    private String type;
}
