package com.stylefeng.guns.rest.modular.websocket.vo;

import com.stylefeng.guns.api.websocket.vo.MessageVO;

public interface ChatServiceAPI{
    void test();

    void saveDemo(MessageVO demoEntity);

    void removeDemo(Long id);

    void updateDemo(MessageVO demoEntity);

    MessageVO findDemoById(Long id);
}
