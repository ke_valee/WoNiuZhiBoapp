package com.stylefeng.guns.rest.common.persistence.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.rest.common.persistence.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author stylefeng
 * @since 2017-08-23
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
    //更新在线观看人数
    @Update("update snail_studio set onlineNum = #{onlineNum} where guid = #{guid}")
    int updateOnlineNum(@Param("onlineNum") int onlineNum,@Param("guid")String guid);
}