package com.stylefeng.guns.rest.common.persistence.dao;

import com.stylefeng.guns.rest.common.persistence.model.SnailUserExp;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户信息拓展表 Mapper 接口
 * </p>
 *
 * @author qifanlee
 * @since 2019-04-04
 */
public interface SnailUserExpMapper extends BaseMapper<SnailUserExp> {

    int insertUserExp(SnailUserExp snailUserExp);
}
