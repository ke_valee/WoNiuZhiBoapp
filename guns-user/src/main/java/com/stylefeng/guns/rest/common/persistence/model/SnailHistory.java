package com.stylefeng.guns.rest.common.persistence.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author qifanlee
 * @since 2019-06-09
 */
@TableName("snail_history")
@Data
public class SnailHistory extends Model<SnailHistory> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 直播的封面图地址
     */
    @TableField("head_addr")
    private String headAddr;
    /**
     * 直播标题
     */
    private String title;
    /**
     * 主播名字
     */
    private String name;
    /**
     * 分类
     */
    @TableField("category_name")
    private String categoryName;
    /**
     * 用户id
     */
    @TableField("user_id")
    private String userId;

    /**
     * 直播地址
     */
    @TableField("live_addr")
    private String liveAddr;

    private Date time;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHeadAddr() {
        return headAddr;
    }

    public void setHeadAddr(String headAddr) {
        this.headAddr = headAddr;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SnailHistory{" +
        "id=" + id +
        ", headAddr=" + headAddr +
        ", title=" + title +
        ", name=" + name +
        ", categoryName=" + categoryName +
        ", time=" + time +
        "}";
    }
}
