package com.stylefeng.guns.rest.common.persistence.dao;

import com.stylefeng.guns.api.user.vo.UserDetailInfo;
import com.stylefeng.guns.rest.common.persistence.model.SnailUser;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author qifanlee
 * @since 2019-04-15
 */
@Mapper
public interface SnailUserMapper extends BaseMapper<SnailUser> {
    //更改用户实名认证状态
    @Update("update snail_user set cert_status =1 where UUID = #{uuid}")
    int updateCertStatus(Integer uuid);

    //查看个人信息
//    @Select("SELECT\n" +
//            "\tu.nickname,\n" +
//            "\tu.username,\n" +
//            "\tu.head_addr AS head_path,\n" +
//            "\tue.sex,\n" +
//            "\tue.area AS address,\n" +
//            "\tue.birthday,\n" +
//            "\ts.guid AS room_id\n" +
//            "FROM\n" +
//            "\tsnail_user u,\n" +
//            "\tsnail_user_exp ue,\n" +
//            "\tsnail_studio s\n" +
//            "WHERE\n" +
//            "\tu.UUID = ue.uid\n" +
//            "AND s.uid = ue.uid\n" +
//            "AND ue.uid = #{userId}")
    UserDetailInfo getUserDetail(int userId);

    //获取粉丝数
    @Select("select count(*) from snail_focus where to_user_id = #{uid}")
    Integer getFansNum(int uid);
    //获取订阅数
    @Select("select count(*) from snail_focus where from_user_id = #{uid}")
    Integer getFocusNum(int uid);

    @Select("select * from snail_user where UUID = #{uid}")
    SnailUser selectUserById(int uid);

    @Select("SELECT\n" +
            "\tnickname\n" +
            "FROM\n" +
            "\tsnail_user u,\n" +
            "\tsnail_studio  s\n" +
            "WHERE\n" +
            "\ts.guid = #{guid}\n" +
            "AND s.uid = u.UUID")
    String selectUserByGuid(String guid);


}
