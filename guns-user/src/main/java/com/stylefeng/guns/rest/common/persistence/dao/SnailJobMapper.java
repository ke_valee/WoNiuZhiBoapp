package com.stylefeng.guns.rest.common.persistence.dao;

import com.stylefeng.guns.api.user.vo.JobList;
import com.stylefeng.guns.rest.common.persistence.model.SnailJob;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author qifanlee
 * @since 2019-06-11
 */
@Mapper
public interface SnailJobMapper extends BaseMapper<SnailJob> {
    @Select("SELECT\n" +
            "\tj.id as id,\n" +
            "\tj.job_name as jobName,\n" +
            "\tj.job_desc as jobDesc,\n" +
            "\tj.score,\n" +
            "\tj.num,\n" +
            "\td.had_done as hadDone\n" +
            "FROM\n" +
            "\tsnail_job j,\n" +
            "\tsnail_job_detail d\n" +
            "WHERE\n" +
            "\tj.id = d.job_id\n" +
            "AND j.type = 1\n" +
            "AND d.user_id = #{uid}")
    //获取用户任务
    List<JobList> getUserJobs(int uid);

    @Select("SELECT\n" +
            "\tj.id as id,\n" +
            "\tj.job_name as jobName,\n" +
            "\tj.job_desc as jobDesc,\n" +
            "\tj.score,\n" +
            "\tj.num,\n" +
            "\td.had_done as hadDone\n" +
            "FROM\n" +
            "\tsnail_job j,\n" +
            "\tsnail_job_detail d\n" +
            "WHERE\n" +
            "\tj.id = d.job_id\n" +
            "AND j.type = 2\n" +
            "AND d.user_id = #{uid}")
        //获取主播任务
    List<JobList> getAouJobs(int uid);

    @Select("SELECT\n" +
            "\tj.id as id,\n" +
            "\tj.job_name as jobName,\n" +
            "\tj.job_desc as jobDesc,\n" +
            "\tj.score,\n" +
            "\tj.num,\n" +
            "\td.had_done as hadDone\n" +
            "FROM\n" +
            "\tsnail_job j,\n" +
            "\tsnail_job_detail d\n" +
            "WHERE\n" +
            "\tj.id = d.job_id\n" +
            "AND j.type = 3\n" +
            "AND d.user_id = #{uid}")
        //获取活动任务
    List<JobList> getActJobs(int uid);

    @Select("select id from snail_job where type !=3")
    List<Integer> getJobIds();
}
