package com.stylefeng.guns.rest.common.persistence.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 用户信息拓展表
 * </p>
 *
 * @author qifanlee
 * @since 2019-04-04
 */
@TableName("snail_user_exp")
public class SnailUserExp extends Model<SnailUserExp> {

    private static final long serialVersionUID = 1L;

    /**
     * 关联user表的主键
     */
    private Integer uid;
    /**
     * 真实姓名
     */
    private String realname;
    /**
     * 身份证号
     */
    private String idcard;
    /**
     * 性别
     */
    private String sex;
    /**
     * 身份证所在地
     */
    private String area;
    /**
     * 省
     */
    private String province;
    /**
     * 市
     */
    private String city;
    /**
     * 区县
     */
    private String prefecture;
    /**
     * 出生年月
     */
    private String birthday;
    /**
     * 地区代码
     */
    private String addrCode;
    /**
     * 身份证校验码
     */
    private String lastCode;


    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPrefecture() {
        return prefecture;
    }

    public void setPrefecture(String prefecture) {
        this.prefecture = prefecture;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAddrCode() {
        return addrCode;
    }

    public void setAddrCode(String addrCode) {
        this.addrCode = addrCode;
    }

    public String getLastCode() {
        return lastCode;
    }

    public void setLastCode(String lastCode) {
        this.lastCode = lastCode;
    }


    @Override
    public String toString() {
        return "SnailUserExp{" +
        " uid=" + uid +
        ", realname=" + realname +
        ", idcard=" + idcard +
        ", sex=" + sex +
        ", area=" + area +
        ", province=" + province +
        ", city=" + city +
        ", prefecture=" + prefecture +
        ", birthday=" + birthday +
        ", addrCode=" + addrCode +
        ", lastCode=" + lastCode +
        "}";
    }

    @Override
    protected Serializable pkVal() {
        return null;
    }
}
