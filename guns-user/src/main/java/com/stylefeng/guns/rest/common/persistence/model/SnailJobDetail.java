package com.stylefeng.guns.rest.common.persistence.model;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author qifanlee
 * @since 2019-06-12
 */
@TableName("snail_job_detail")
public class SnailJobDetail extends Model<SnailJobDetail> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 任务id
     */
    @TableField("job_id")
    private Integer jobId;
    /**
     * 已完成任务数
     */
    @TableField("had_done")
    private Integer hadDone;
    /**
     * 用户id
     */
    @TableField("user_id")
    private Integer userId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

    public Integer getHadDone() {
        return hadDone;
    }

    public void setHadDone(Integer hadDone) {
        this.hadDone = hadDone;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SnailJobDetail{" +
        "id=" + id +
        ", jobId=" + jobId +
        ", hadDone=" + hadDone +
        ", userId=" + userId +
        "}";
    }
}
