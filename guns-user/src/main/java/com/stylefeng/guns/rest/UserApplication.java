package com.stylefeng.guns.rest;

import com.alibaba.dubbo.spring.boot.annotation.EnableDubboConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;


import javax.annotation.PostConstruct;

@SpringBootApplication(scanBasePackages = {"com.stylefeng.guns"})
@EnableDubboConfiguration
@EnableCaching
public class UserApplication {

    @Autowired
    private RedisConnectionFactory redisConnectionFactory = null;

    @Autowired
    private RedisTemplate redisTemplate = null;

    //自定义初始化
    @PostConstruct
    public void init(){
        initTemplate();
    }

    //改变RedisTemplate对于键的序列化策略
    private void initTemplate(){
        RedisSerializer stringSerializer = redisTemplate.getStringSerializer();
        redisTemplate.setKeySerializer(stringSerializer);
        redisTemplate.setHashKeySerializer(stringSerializer);
    }

    public static void main(String[] args) {

        SpringApplication.run(UserApplication.class, args);
    }
}
