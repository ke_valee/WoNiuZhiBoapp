# 蜗牛直播app

#### 介绍
这是和伙伴合作完成的直播app，一个安卓，一个后台，还在继续完善。后台采用基于springboot的guns开源框架搭建，设计为直播服务，user服务，websocket服务以及API网关服务。数据库使用了mongodb,redis和mysql。 比较有看头的是采用websocket+mongodb搭建了多人直播聊天室，用redis搭建了热搜排行榜等。在直播服务器上，选择了SRS，搭建简单，功能强大。开了两台学生机阿里云服务器，一台机的话顶不住srs+几个微服务。

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)